import React,{FunctionComponent,} from 'react';
import { BrowserRouter as Router, Switch,Link, Route} from 'react-router-dom';
import PageNotFound from './pages/page-not-found';
import PokemonAdd from './pages/pokemon-add';
import PokemonsDetail from './pages/pokemon-detail';
import PokemonEdit from './pages/pokemon-edit';
import PokemonList from './pages/pokemon-list';



const App: FunctionComponent = () => {


return (
    <Router>
        <div>
            {/* la baree de navigation commun à toutes les pages */}
            <nav>
                <div className="nav-wrapper teal">
                    <Link to="/" className="brand-logo center">Pokédex</Link>
                </div>
            </nav>
            {/* Le système de gestion des routes de notre application*/}
            <Switch>
                <Route exact path="/" component={PokemonList}/>
                <Route exact path="/pokemons" component={PokemonList}/>
                <Route exact path="/pokemons/add" component={PokemonAdd}/>
                <Route exact path="/pokemons/edit/:id" component={PokemonEdit}/>
                <Route path="/pokemons/:id" component={PokemonsDetail}/>
                <Route component={PageNotFound}/>
            </Switch>
        </div>
    </Router>
)
}
  
export default App;